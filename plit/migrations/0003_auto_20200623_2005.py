# Generated by Django 3.0.7 on 2020-06-23 14:35

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('plit', '0002_auto_20200618_2246'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='plins',
            name='x',
        ),
        migrations.AddField(
            model_name='plins',
            name='descri',
            field=models.CharField(default='___', max_length=3000),
        ),
    ]
