from django.shortcuts import render
from . import models
from django.conf import settings
from django.conf.urls.static import static
from django.core.mail import send_mail
from plit.models import plins
from django.utils.timezone import datetime
from django.shortcuts import render


# Create your views here.
def index(request):
	today = datetime.strftime(datetime.today(),"%Y-%m-%d")
	today = datetime.strptime(today,"%Y-%m-%d")
	data = plins.objects.filter(date__day = today.day,date__month= today.month)
	return render(request,'index.html',{'data':data ,'media_url':settings.MEDIA_URL
		})




def Contact(request):
    



    return render(request,'user info.html')

def ffsm(request):
	regobj = models.plins()  
	phone1 = request.POST.get('bshd')
	name1 = request.POST.get('firstname')
	compn1 = request.FILES.get('username')
	dcrp =  request.POST.get('descri')

	regobj.date = phone1
	regobj.name = name1
	regobj.image = compn1
	regobj.about = dcrp

	regobj.save()
	return render(request,'user info_ts.html')
def About(request):
    return render(request,'about.html')

def products(request):
    return render(request,'products.html')    

def image_upload_view(request):
    """Process images uploaded by users"""
    if request.method == 'POST':
        form = ImageForm(request.POST, request.FILES)
        if form.is_valid():
            form.save()
            # Get the current instance object to display in the template
            img_obj = form.instance
            return render(request, 'user info.html', {'form': form, 'img_obj': img_obj})
    else:
        form = ImageForm()
    return render(request, 'user info.html', {'form': form})




